class Request {
    private String request;
    private AlexaDevice requestingDevice;

    Request(String request, AlexaDevice requestingDevice) {
        this.request = request;
        this.requestingDevice = requestingDevice;
    }

    String getRequest() {
        return request;
    }

    void setRequest(String request) {
        this.request = request;
    }

    AlexaDevice getRequestingDevice() {
        return requestingDevice;
    }

    void setRequestingDevice(AlexaDevice requestingDevice) {
        this.requestingDevice = requestingDevice;
    }
}
