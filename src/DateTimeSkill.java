import java.time.LocalDateTime;

public class DateTimeSkill extends AbstractSkill {

    @Override
    public void run() {
        System.out.println("The time is: " + LocalDateTime.now());
    }
}
