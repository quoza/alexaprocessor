import java.util.Observable;
import java.util.Observer;

public class AlexaServer implements Observer {

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof Request){
            Request request = (Request) arg;
            AbstractSkill callback = parseRequest(request);
            request.getRequestingDevice().invoke(callback);
        }
    }

    AbstractSkill parseRequest(Request request){
        String req = request.getRequest();
        req = req.toLowerCase();
        if (req.startsWith("alexa")){
            req = req.substring(6);
            if (req.startsWith("add todo task")) {
                System.out.println("Adding a ToDo task...");
            }
            if (req.startsWith("remove todo task")){
                System.out.println("Removing the ToDo task...");
            }
            if (req.startsWith("list tasks")){
                System.out.println("Listing tasks...");
            }
            if (req.startsWith("what time is it")
                    || req.startsWith("gimme the time")
                    || req.startsWith("the time")){
                System.out.println("Returning the time...");
                return new DateTimeSkill();
            }
        }
        return new DontKnowSkill();
    }
}
