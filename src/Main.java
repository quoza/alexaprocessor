import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        AlexaDevice device = new AlexaDevice();
        AlexaServer server = new AlexaServer();
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine()){
            String line = sc.nextLine();
            if (line.equalsIgnoreCase("register")){
                if (device.countObservers() == 0){
                    device.addObserver(server);
                    System.out.println("Podlaczono Internet.");
                }
            } else if (line.equalsIgnoreCase("unregister")){
                device.deleteObserver(server);
                System.out.println("Odlaczono Internet.");
            } else {
                device.sendRequest(line);
            }
        }
    }
}
