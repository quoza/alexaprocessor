import java.util.Observable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class AlexaDevice extends Observable {
    private static int alexaIds;
    private int id = alexaIds;
    private ExecutorService executorService = Executors.newSingleThreadExecutor();


    AlexaDevice() {
    }

    void sendRequest(String request){
        if(countObservers() == 0){
            System.out.println("Sorry, I'm not connected to the Internet.");
            return;
        }
        if (request.toLowerCase().startsWith("alexa")){
            setChanged();
            notifyObservers();
        }
    }

    void invoke(AbstractSkill callback){
        executorService.submit(callback);
    }
}
